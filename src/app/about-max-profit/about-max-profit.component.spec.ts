import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutMaxProfitComponent } from './about-max-profit.component';

describe('AboutMaxProfitComponent', () => {
  let component: AboutMaxProfitComponent;
  let fixture: ComponentFixture<AboutMaxProfitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutMaxProfitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutMaxProfitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
