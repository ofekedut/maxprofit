import { Component, OnInit } from '@angular/core';
import { Inject, Injectable } from "@angular/core";
import { LOCAL_STORAGE, StorageService } from "ngx-webstorage-service";
import { ApiService } from '../api.service';
import { debug } from 'console';

@Component({
  selector: 'app-academy',
  templateUrl: './academy.component.html',
  styleUrls: ['./academy.component.css']
})
export class AcademyComponent implements OnInit {
  constructor(private api : ApiService) { }
  response
  agreedGuidelines
  condition = true
  coursesChoosen = false
  strategiesChoosen = true
  strategiesBought
  coursesBought 
  logged 
  strategiesAllowed
  courseAllowed 
  course
  number
  subNumber
  CourseAllowed(num)
  {
    
    if (num in this.coursesBought)
    {
      return true
    }
    else 
    {
      return false
    }
  }
  StrategiesAllowed(num)
  {
    
    if (num in this.strategiesBought)
    {
      return true
    }
    else 
    {
      return false
    }
  }

  async StrategiesBought()
  {
    if (localStorage.getItem('logged') == 'true')
    {
      let res = await this.api.Users.StrategiesBought(localStorage.getItem('loggedEmail'))
      let data = await res.json()
      let strategiesBought = data.strategies 
      this.strategiesBought = strategiesBought
    }
    else 
    this.strategiesBought = []
  }
  async CoursesBought()
  {
    if (localStorage.getItem('logged') == 'true')
    {
      let res = await this.api.Users.CoursesBought(localStorage.getItem('loggedEmail'))
      let data = await res.json()
      let coursesBought = data.courses 
      this.coursesBought = coursesBought
    }
    else 
    this.coursesBought = []
  }
  async GetAgreed(email: string){
    if (email == null)
    {return}
    let res = await this.api.GeneralInfo.AgreedGuidelines(email)
    let data = await res.json()
    let cond = data.agreed 
    if (cond = 'false')
    {return false}
    else
    {return true}
  }
  async coursesOn(){
    this.coursesChoosen = true
    this.strategiesChoosen = false
    await this.CoursesBought()
    var cour = this.coursesBought
    if (cour.length == 0)
    {this.courseAllowed= false} 
    else {this.courseAllowed = true}
  }
  async strategiesOn(){
    this.coursesChoosen=false
    this.strategiesChoosen=true
    await this.StrategiesBought()
    var strat = this.strategiesBought
    if (strat.length == 0)
    {this.strategiesAllowed= false}
    else{ this.strategiesAllowed=true}
  }
  async ngOnInit() {
    await this.StrategiesBought()
    if (localStorage.getItem('logged') == 'true')
    {
      this.logged = true
      this.agreedGuidelines = this.GetAgreed(localStorage.getItem('loggedEmail'))
    }
    else 
    {
      this.logged=false
      this.agreedGuidelines = false
    }
  }
  owned(x){
    x= String(x)
    if (     this.coursesBought.indexOf(x) > -1
    )
    {
      return true
    }
    return false
  }
}
