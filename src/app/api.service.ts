import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {from} from 'rxjs';
import { ContactForm } from './contact-form';
@Injectable({
  providedIn: 'root'
})

export class ApiService {
  static url: string = 'https://dbrgi8kjdd.execute-api.eu-central-1.amazonaws.com/prod'
  constructor(public httpClient: HttpClient) { }




  public GeneralInfo = new class{
    AgreedGuidelines(email: string)
    {
      return fetch (
        ApiService.url+'?main_task=general_info&sub_task=agreed_guidelines&email='+email,
        {
          headers:{},
          method: "GET",
          mode:'cors'
        }
      )
    }
    UsersCount(){
      return fetch (
        ApiService.url+'?main_task=general_info&sub_task=users_count',
        {
          headers: {},
          method: 'GET',
          mode: 'cors'
        }
      )
    }
  }


  public Contact = new class{
    } 



  public Users = new class{
    SendNewPass(email, new_pass)
    {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      var raw = JSON.stringify({"main_task":"log","sub_task":"change_pass","email":email, 'password':new_pass});
      return fetch ("https://dbrgi8kjdd.execute-api.eu-central-1.amazonaws.com/prod", {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        mode: 'cors'
      }) 
    }
    CheckChangePassCode(email, code){
      return fetch( ApiService.url+'?main_task=general_info&sub_task=user_exists'+'&email='+email+'&key='+code,
      {
        headers: {},
        method: "GET",
        mode:"cors"
      }
      )
    }
    CheckIfUserMailExists(email){
      return fetch( ApiService.url+'?main_task=general_info&sub_task=user_exists'+'&email='+email,
      {
        headers: {},
        method: "GET",
        mode:"cors"
      }
      )
    }
    SendChangePassCodeToMail(email)
    {
      var myHeaders = new Headers();
      var Response;
      myHeaders.append("Content-Type", "application/json");
      var raw = JSON.stringify({"main_task":"log","sub_task":"forgot_pass","email":email});
      return fetch ("https://dbrgi8kjdd.execute-api.eu-central-1.amazonaws.com/prod", {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        mode: 'cors'
      }) 
    }
    CoursesBought(email){
      return fetch( ApiService.url+'?main_task=general_info&sub_task=courses_bought'+'&email='+email,
      {
        headers: {},
        method: "GET",
        mode:"cors"
      }
      )
    }
    StrategiesBought(email){
      return fetch( ApiService.url+'?main_task=general_info&sub_task=strategies_bought'+'&email='+email,
      {
        headers: {},
        method: "GET",
        mode:"cors"
      }
      )
    }

    GetUserInfo(email){
      return fetch (
        ApiService.url+'?main_task=log&sub_task=user_info'+'&email='+email,
      {
        headers: {},
        method: 'GET',
        mode: 'cors'
      }
      )

    }
  }
  getLasrReviews()
  {
    return fetch (
      ApiService.url+'?main_task=review&sub_task=get_first_reviews',
        {
          headers: {},
          method: 'GET',
          mode: 'cors'
        }, 
      )
  }

  addReview(email, text)
  {
    return from 
    (fetch(
      ApiService.url,
      {
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'POST',
        mode: 'cors', // the most important option
        body: JSON.stringify({
          "main_task": "review",
          "sub_task" : "add_review",
          "email": email,
          "text": text,
          "POST": true
        })
      }
    ))
  }



  addUser(fullName, gender, ID, phone, email, notes, pass){
    var myHeaders = new Headers();
    var Response;
    myHeaders.append("Content-Type", "application/json");
    var raw = JSON.stringify({"main_task":"log","sub_task":"add_user","full_name":fullName,"gender":gender,"email":email,"id":ID,"phone":phone,"notes":notes,"password":pass});
    return fetch ("https://dbrgi8kjdd.execute-api.eu-central-1.amazonaws.com/prod", {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      mode: 'cors'
    })
  }

  login(email: string, password: string)
  {
    return fetch(
        ApiService.url + "?main_task=log&sub_task=login&email="+email+"&password="+password, // the url you are trying to access
        {
          headers: {},
          method: 'GET',
          mode: 'cors'
        })
  }
}