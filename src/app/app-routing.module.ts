import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomePageComponent} from './home-page/home-page.component';
import { SignupComponent } from "./signup/signup.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { GuidelinesComponent } from "./guidelines/guidelines.component";
import { AcademyComponent } from "./academy/academy.component";
import {  AboutMaxProfitComponent} from "./about-max-profit/about-max-profit.component";
import { FeedbackComponent } from "./feedback/feedback.component";
import {JourneyComponent  } from "./journey/journey.component";
import { LoginComponent } from "./login/login.component";
import {MetaTraderComponent} from "./meta-trader/meta-trader.component"
import { ReviewsComponent} from "./reviews/reviews.component"
import { RunMyBizComponent } from "./run-my-biz/run-my-biz.component";
import { TebComponent } from "./teb/teb.component";
import {MostFamousComponent} from "./most-famous/most-famous.component"
import { StepByStepComponent } from './step-by-step/step-by-step.component';
import { AvoidDistractionsComponent } from "./avoid-distractions/avoid-distractions.component";
import { ReducedPricesComponent } from "./reduced-prices/reduced-prices.component";
import { BitCheckoutComponent } from "./bit-checkout/bit-checkout.component";
import { CoursesComponent } from "./courses/courses.component";
import { GuildelinesAcademyComponent } from "./guildelines-academy/guildelines-academy.component";
import {StrategiesComponent} from "./strategies/strategies.component"
import { ContactComponent } from "./contact/contact.component";
import {AdminzoneComponent} from "./adminzone/adminzone.component"
import {ChangepassComponent} from "./changepass/changepass.component"
const routes: Routes = [
  {path: '', component: HomePageComponent},
  {path: 'most-famous', component: MostFamousComponent },
  {path: 'step-by-step', component: StepByStepComponent },
  {path: 'avoid-distractions', component: AvoidDistractionsComponent },
  {path: 'reduced-prices', component: ReducedPricesComponent},
  {path: 'bit-checkout', component: BitCheckoutComponent },
  { path: 'reviews', component: ReviewsComponent},
  {path: 'academy', component:AcademyComponent},
  {path: 'academy-guidelines', component: GuildelinesAcademyComponent},
    {path: 'courses', component: CoursesComponent}, 
    {path: 'strategies', component: StrategiesComponent},
  {path : 'signup', component: SignupComponent},
  {path : 'guidelines', component: GuidelinesComponent},
  {path : 'not-found', component: PageNotFoundComponent},
  {path: 'about', component :AboutMaxProfitComponent},
  {path: 'feedback', component :FeedbackComponent},
  {path: 'journey', component :JourneyComponent},
  {path: 'login', component :LoginComponent},
  {path: 'meta-trader', component :MetaTraderComponent},
  {path: 'rin-my-biz', component :RunMyBizComponent},
  {path: 'teb', component :TebComponent},
  {path: 'contact', component :ContactComponent},
  {path: 'adminzone', component: AdminzoneComponent},
  {path:'changepass', component:ChangepassComponent},
  {path : '**', redirectTo: '/not-found'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
