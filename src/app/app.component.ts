import { Component, OnInit } from '@angular/core';
import { User } from './user';
import { Inject, Injectable } from "@angular/core";
import { LOCAL_STORAGE, StorageService } from "ngx-webstorage-service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})

export class AppComponent implements OnInit{
  
  title = 'maxProfit';
  logged : boolean = this.getlogged()
  hidemenu: boolean
  menuClicked()
  {
    this.hidemenu = false
  }
  menuChoiceMade()
  {
    this.hidemenu = true
  }

  getlogged(){
    var a =localStorage.getItem('logged')
    if (a == 'false')
      {return false}
    else {return true}
  }
  static user = new User ('', false)
  ngOnInit()
  { 
    this.getlogged()
    this.hidemenu = true
  }
  getUser(){return localStorage.getItem('username')} 
  username = this.getUser()
  Logout()
  {
    localStorage.setItem("logged", "false")
    localStorage.setItem("loggedEmail", "")
  }
}