  import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { HomePageComponent } from './home-page/home-page.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AcademyComponent } from './academy/academy.component';
import { JourneyComponent } from './journey/journey.component';
import { RunMyBizComponent } from './run-my-biz/run-my-biz.component';
import { TebComponent } from './teb/teb.component';
import { MetaTraderComponent } from './meta-trader/meta-trader.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { LoginComponent } from './login/login.component';
import { AboutMaxProfitComponent } from './about-max-profit/about-max-profit.component';
import { ReviewsComponent } from './reviews/reviews.component';
import { GuidelinesComponent } from './guidelines/guidelines.component';
import { MostFamousComponent } from './most-famous/most-famous.component';
import { AvoidDistractionsComponent } from './avoid-distractions/avoid-distractions.component';
import { StepByStepComponent } from './step-by-step/step-by-step.component';
import { ReducedPricesComponent } from './reduced-prices/reduced-prices.component';
import { BitCheckoutComponent } from './bit-checkout/bit-checkout.component';
import { ContactComponent } from './contact/contact.component';
import { CoursesComponent } from './courses/courses.component';
import { GuildelinesAcademyComponent } from './guildelines-academy/guildelines-academy.component';
import { StrategiesComponent } from './strategies/strategies.component';
import {StorageServiceModule} from 'ngx-webstorage-service'
import {MatMenuModule, MatButtonModule, MatIconModule} from '@angular/material'
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NavbarComponent } from './navbar/navbar.component';
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatSidenavModule} from '@angular/material';
import { AdminzoneComponent } from './adminzone/adminzone.component';
import { ChangepassComponent } from './changepass/changepass.component';

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    HomePageComponent,
    PageNotFoundComponent,
    AcademyComponent,
    JourneyComponent,
    RunMyBizComponent,
    TebComponent,
    MetaTraderComponent,
    FeedbackComponent,
    LoginComponent,
    AboutMaxProfitComponent,
    ReviewsComponent,
    GuidelinesComponent,
    MostFamousComponent,
    AvoidDistractionsComponent,
    StepByStepComponent,
    ReducedPricesComponent,
    BitCheckoutComponent,
    ContactComponent,
    CoursesComponent,
    GuildelinesAcademyComponent,
    StrategiesComponent,
    NavbarComponent,
    AdminzoneComponent,
    ChangepassComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    StorageServiceModule,
    MatButtonModule,
    MatMenuModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
