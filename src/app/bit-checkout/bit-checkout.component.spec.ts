import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BitCheckoutComponent } from './bit-checkout.component';

describe('BitCheckoutComponent', () => {
  let component: BitCheckoutComponent;
  let fixture: ComponentFixture<BitCheckoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BitCheckoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BitCheckoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
