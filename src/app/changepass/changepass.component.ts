import { Component, OnInit } from '@angular/core';
import {DataService} from "../data.service"
import {  ApiService} from "../api.service";
import { Inject, Injectable } from "@angular/core";
import { LOCAL_STORAGE, StorageService } from "ngx-webstorage-service";
import { GetUserParameterToLocalStorage } from "../login";
import { JsonPipe } from '@angular/common';

@Component({
  selector: 'app-changepass',
  templateUrl: './changepass.component.html',
  styleUrls: ['./changepass.component.css']
})
export class ChangepassComponent implements OnInit {

  form
  message
  logged
  change
  secretKey
  newPass
  validCode
  constructor(private api : ApiService) { }

  async sendNewPass(){
    var res = await this.api.Users.SendNewPass(this.form.email, this.newPass)
    var data = await res.json();
    this.message = 'הסיסמה שונתה בהצלחה'
  }
  async sendCode(){
    var res = await this.api.Users.SendChangePassCodeToMail(this.form.email)
    var data = await res.json();
    if (data.valid == '1')
    {
      this.validCode = true
    }
    else
    {
      this.validCode = false
      this.message = "הקוד שהוכנס שגוי"
    }
  }
  async CheckIfUserMailExists()
  {
    var res = await this.api.Users.CheckIfUserMailExists(this.form.email)
    var data = await res.json();
    var result = data.text
    debugger
    if (result == '0')
      {
        this.message = 'המייל אינו קיים במערכת, אנא צרו קשר'
      }
  }
  async SendChangePassCodeToMail(){
    var res = await this.api.Users.SendChangePassCodeToMail(this.form.email)
    var data = await res.json();
    if ('text' in data)
    {
      if (data.text == 'mail_sent')
      {this.message = 'קוד לשחזור סיסמה נשלח אליכם למייל, אנא הכניסו אותו'
      this.change  = true}
    }
  }
  async onSubmit(){
    await this.CheckIfUserMailExists
    await this.SendChangePassCodeToMail()
  }
  ngOnInit() {
    this.change = false
    this.logged = localStorage.getItem("logged")
    if (this.logged == 'true')
    {
      this.logged = true
      this.message = "ההתחברות בוצעה בהצלחה! \n שלום"  +  (localStorage.getItem('username')) }
    else{
      this.logged =false
    this.message = "" 
    }
    this.form = {
      'email': ''
    }
  }

}
