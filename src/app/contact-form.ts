import { ApiService } from "./api.service";

export class ContactForm {
    constructor(public tel: string, public email: string, public message: string, private api : ApiService){
    }

    submit(){
            return fetch(
              ApiService.url,
              {
                headers: {
                  'Content-Type': 'application/json',
                },
                method: 'POST',
                mode: 'cors', // the most important option
                body: JSON.stringify({
                  "main_task": "contact",
                  "sub_task" : "send_message",
                  "email": this.email,
                  "message": this.message,
                  "phone": this.tel
                })
          })
    }
}

