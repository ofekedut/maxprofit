import { Component, OnInit } from '@angular/core';
import { ContactForm } from "../contact-form";
import { ApiService } from '../api.service';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  constructor (private api : ApiService){}
  contactForm = new ContactForm("", "", "", this.api)
  nirDisp
  yaromDisp
  nirContact(){
    this.nirDisp = true;
    this.yaromDisp = false;
  }
  yaromContact(){
    this.nirDisp = false;
    this.yaromDisp = true;
  }
  ngOnInit()
  {
    this.yaromDisp = false;
    this.nirDisp = false;
  }
  onSubmit(){
    this.contactForm.submit()
  }
}
