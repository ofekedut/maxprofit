import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  url = 'https://dbrgi8kjdd.execute-api.eu-central-1.amazonaws.com/prod'
  constructor() { }
  LoginUser(email, password){
    return fetch(
      this.url + "?main_task=log&sub_task=login&email="+email+"&password="+password, // the url you are trying to access
      {
        headers: {
        },
        method: 'GET',
        mode: 'cors'
      })
  }
}
