import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuildelinesAcademyComponent } from './guildelines-academy.component';

describe('GuildelinesAcademyComponent', () => {
  let component: GuildelinesAcademyComponent;
  let fixture: ComponentFixture<GuildelinesAcademyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuildelinesAcademyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuildelinesAcademyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
