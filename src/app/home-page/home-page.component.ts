import { Component, OnInit , Input} from '@angular/core';
import { Inject, Injectable } from "@angular/core";
import { LOCAL_STORAGE, StorageService } from "ngx-webstorage-service";
import { ApiService } from '../api.service';

class guidelines{
  static get_guidelines ()
  {
    var list1: Array<string> = ["הסכם שימוש בטלגרם של - Maxprofit | TEB: 📋   אנו נוטים לנתח את כל צמדי המטבעות שאנו חושבים שהינן בנות ניתוח ומזמינות פעולת קנייה או מכירה בהסתכלות טכנית. "
    ,"אנו עושים זאת בזהירות, תוך כדי התחשבות במגמת המסחר הכללית, במחזורי המסחר והנזילות הממוצעת, סטיות התקן, תנאי השוק הגלובליים והתחשבות בסקטור הכללי והכי חשוב: בזמן אמת!"
    ,"אני מציע לכל אחד ואחת , לפני שאתם פועלים בניירות אלה, לבדוק האם יחס הסיכוי/סיכון מתאים לסגנון המסחר שאתם רגילים אליו או האם בכלל מלכתחילה אתם מתאימים למסחר."
    ,"כל מידע שמסופק כאן הינו אך ורק דעתו האישית של הכותב.","המנהלים או חברי הפורום אינם בעלי רישיון השקעות או כל רישיון פיננסי אחר."
    ,"המידע בפורום אינו מהווה תחליף לייעוץ מקצועי המתחשב בצרכיו האישיים של כל אדם."
    ,"ובמקום זאת, אנחנו פועלים כפלטפורמה חינוכית להחלפת מידע על פורקס."
    ,"כל מידע שיחשף, בין אם במפורש או במשתמע, אודות רווח  או הכנסות, אין זה ערובה. "
    ,"אף שיטה או מערכת מסחר לא מבטיחה שהיא תניב רווח, שכן עלולה אף להוסיף להפסד כסף."
    ,"כותבי הסקירות, הניתוחים (וכו') יכולים להחזיק בחלק מצמדי המטבעות - במקרה הנ״ל נציין זאת כגילוי נאות."
    ,"אנא חברים, כל העושה צורך במידע המועבר בפורום זה או הקשורות אליו עושה זאת על דעת עצמו ועל אחריותו הבלעדית!"
    ,"חברי הפורום רשאים לעזוב את הפורום בכל עת לפי בחירתם, בהישארות בפורום מביע חבר הפורום את הסכמתו לתנאי השימוש ולהסרת כל אחריות ממנהלי הפורום ו/ או כל חבר אחר בפורום באשר לפעולות ו / או הימנעות מפעולות שיבוצעו על ידו."
    ,"זמינים לכל שאלה או בקשה לניתוח: "
    ,"Teb@Maxprofit.support"]

    let guidelines = "";
    for (var line of list1)
    {
      guidelines = guidelines+ "\n" + line
    } 
  return guidelines
  }
}
@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})


export class HomePageComponent implements OnInit {
  needSurvey = true
  guide = guidelines.get_guidelines()
  constructor(private api: ApiService) { }
  count
  isAdmin : boolean = false
  logged : boolean = this.getlogged()
  getUser(){return localStorage.getItem('username')} 
  username = this.getUser()
  async UsersCount()
  {
    let res = await this.api.GeneralInfo.UsersCount()
    let data = await res.json()
    this.count = data.count
  }
  getlogged(){
    var a =localStorage.getItem('logged')
    if (a == 'false')
      {return false}
    else {return true}
  } 
  Logout()
  {
    localStorage.setItem("logged", 'false')
    localStorage.setItem("loggedEmail", "")
    location.reload()
  }
  surveyRoute ()
  {
    if (this.needSurvey == true)
    {return "fit-telegram-survey"}
    else
    {return "academy"}
  }
  async ngOnInit() {
    await this.UsersCount()
    if (localStorage.getItem('tier') == null){
      localStorage.setItem('tier', 'user')
      if (localStorage.getItem('tier') == 'admin') {
        this.isAdmin = true
      }
      else {this.isAdmin = false}
    }
  }

  
  adminbutton(){
    
  }
}
