import { Component, OnInit } from '@angular/core';
import { Survey } from '../survey';

@Component({
  selector: 'app-journey',
  templateUrl: './journey.component.html',
  styleUrls: ['./journey.component.css']
})
export class JourneyComponent implements OnInit {
  survey = new Survey (0,0,0,false,0,'', '', false, '')
  submitted = false
  needSurvey = true
  popfailed = false
  getSurvey()
  {
    return JSON.stringify(this.survey)
  }
  constructor() { }
  onSubmit()
  {
    this.submitted = true;
    var result = this.survey.calc_points()
    if (result.points >= 60)
    {this.needSurvey = false}
    else
    {
      this.popfailed=true
    }
  }
  closePop()
  {
    this.popfailed = false
  }
  ngOnInit() {
  }

}
