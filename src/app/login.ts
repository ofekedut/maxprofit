import { ApiService } from "./api.service";
import { Inject, Injectable } from "@angular/core";
import { LOCAL_STORAGE, StorageService } from "ngx-webstorage-service";
import { JsonPipe } from '@angular/common';


export interface LoginTasks{
    GetUserParameterToLocalStorage(email: string)
}

class LoginTasksImpl implements LoginTasks{
    email: string
    constructor(private api : ApiService){}
    public async GetUserParameterToLocalStorage(email:string)
    {
        let res = await this.api.Users.GetUserInfo(email)
        let data = await res.json()
        localStorage.setItem("tier", data.tier)
        localStorage.setItem('username', data.name)
        return data
    }
}

export function GetUserParameterToLocalStorage(email): LoginTasks {
    return new LoginTasksImpl(email)
}
export class Login {
    constructor(public email: string,public password: string){}
    private logUser : ApiService
}


export class loggedData {
    constructor(public loggedEmail : string){}
}
