import { Component, OnInit } from '@angular/core';
import { Login } from "../login";
import {DataService} from "../data.service"
import {  ApiService} from "../api.service";
import { Inject, Injectable } from "@angular/core";
import { LOCAL_STORAGE, StorageService } from "ngx-webstorage-service";
import { GetUserParameterToLocalStorage } from "../login";
import { JsonPipe } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form = new Login("","") // login for ;
  message // text to display to the user;
  Posts
  logged
  ngOnInit() {
    this.logged = localStorage.getItem("logged")
    if (this.logged == 'true')
    {
      this.logged = true
      this.message = "ההתחברות בוצעה בהצלחה! \n שלום"  +  (localStorage.getItem('username')) }
    else{
      this.logged =false
    this.message = "" 
    }
  }
  async loginasync(){
    let response = await this.dataservise.LoginUser(this.form.email, this.form.password);
    let data = await response.json();
    this.Posts = data;
  }
  async GetUserParameterToLocalStorage()
    {
        let res = await this.api.Users.GetUserInfo(this.form.email)
        let data = await res.json()
        localStorage.setItem("tier", data.tier)
        localStorage.setItem('username', data.info.name)
        return
    }
  constructor(private api : ApiService, private dataservise : DataService) { }
  async onSubmit()
  {
    await this.loginasync();
    var emailStatus = this.Posts.email;
    var passStatus  =this.Posts.password;
    if (emailStatus && passStatus)
    {
      localStorage.setItem("logged", "true")
      localStorage.setItem("loggedEmail", this.form.email)
      await this.GetUserParameterToLocalStorage()
      this.logged = true
      this.message = "ההתחברות בוצעה בהצלחה! \n שלום"  +  (localStorage.getItem('username')) 
    }
    else
    {
      this.message = "שם המשתמש או הסיסמה אינם נכונים, נסו שנית."
  }
}
}