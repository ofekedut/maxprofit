import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetaTraderComponent } from './meta-trader.component';

describe('MetaTraderComponent', () => {
  let component: MetaTraderComponent;
  let fixture: ComponentFixture<MetaTraderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetaTraderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetaTraderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
