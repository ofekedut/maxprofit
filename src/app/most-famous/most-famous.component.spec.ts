import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostFamousComponent } from './most-famous.component';

describe('MostFamousComponent', () => {
  let component: MostFamousComponent;
  let fixture: ComponentFixture<MostFamousComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostFamousComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostFamousComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
