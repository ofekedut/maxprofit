import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReducedPricesComponent } from './reduced-prices.component';

describe('ReducedPricesComponent', () => {
  let component: ReducedPricesComponent;
  let fixture: ComponentFixture<ReducedPricesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReducedPricesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReducedPricesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
