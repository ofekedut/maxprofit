import { Component, OnInit } from '@angular/core';
import { Review } from "../review";
import { ApiService } from "../api.service";

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css']
})


export class ReviewsComponent implements OnInit {
  
  constructor(private api : ApiService) { }

  recent_reviews
  ngOnInit()
  {
    this.getRecent()
  }

 async getRecent(){
    let res = await this.api.getLasrReviews()
    let data = await res.json()
    this.recent_reviews = data
  }

  getCircularReplacer = () => {
    const seen = new WeakSet();
    return (key, value) => {
    if (typeof value === "object" && value !== null) {
        if (seen.has(value)) {
            return;
        }
        seen.add(value);
    }
    return value;
    };
  };
  getRecentDisplay(){
    return JSON.stringify(this.recent_reviews)
  }
  
  review = new Review('','')
  response 
  onSubmit()
  {
    this.response = this.api.addReview(this.review.email, this.review.text).subscribe(data => {this.response = data;})
  }
}
