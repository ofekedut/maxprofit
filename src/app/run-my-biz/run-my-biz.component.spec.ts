import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RunMyBizComponent } from './run-my-biz.component';

describe('RunMyBizComponent', () => {
  let component: RunMyBizComponent;
  let fixture: ComponentFixture<RunMyBizComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RunMyBizComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RunMyBizComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
