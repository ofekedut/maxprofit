export class SignupForm {
    constructor (
        public fullName : string,
        public gender: string,
        public ID: string,
        public phone: string ,
        public email : string,
        public notes: string,
        public pass: string,
        public passRepeat: string,
        public mailRepeat: string){}}
