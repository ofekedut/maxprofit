import { Component, OnInit } from '@angular/core';
import {SignupForm} from '../signup-form'
import { ApiService } from "../api.service";
import { JsonPipe } from '@angular/common';
import { Inject, Injectable } from "@angular/core";
import { LOCAL_STORAGE, StorageService } from "ngx-webstorage-service";
import { GetUserParameterToLocalStorage } from "../login";
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit{
  
  constructor(private logUser : ApiService) { }
  form: SignupForm;
  DisplayMessage : string;
  submitted
  Response
  loggedMessage
  logged
  ngOnInit(){
    this.DisplayMessage = ""
    this.form = new SignupForm('', '', '', '','','','','','') //sigup form
    this.submitted  =false
    this.logged = localStorage.getItem("logged")
    if (this.logged == 'true')
    {
      this.logged = true
      this.loggedMessage = "ההתחברות בוצעה בהצלחה! \n שלום"  +  (localStorage.getItem('username')) }
    else{
      this.logged = false
    this.loggedMessage = "" 
    }
  }
  signUp = async () => {
    try {
      var res = await this.logUser.addUser(this.form.fullName, this.form.gender, this.form.ID,
        this.form.pass,this.form.email,this.form.notes,this.form.pass);
      var data = await res.json();
      this.Response = data ;
    } catch (err) {
      console.error(err);
    }
  }
  async onSubmit()
  {
    this.submitted=true
    if (this.form.mailRepeat != this.form.email)
    {
      this.DisplayMessage = "המיילים שהוזנו אינם תואמים!"
    }
    else if (this.form.pass != this.form.passRepeat)
    {
      this.DisplayMessage = "הסיסמאות שהוזנו אינן תואמות!"
    }
    await this.signUp()
    if (this.Response.hasOwnProperty('error'))
    {
      let error = this.Response.error
      if (error == 'invalid_mail')
      {
        this.DisplayMessage = 'המייל שהוכנס אינו תקין, אנא נסו שוב'
      }
      if (error == 'mail_taken')
        {
          this.DisplayMessage = 'המייל שהוכנס תפוס, שכחת סיסמה? צור קשר עם התמיכה הטכנית '
        }
      if (error == 'invalid_password')
      {
        this.DisplayMessage = 'הסיסמה אינה עונה על התנאים, עליה להכיל לפחות אות אחת גדולה וקטנה באנגלית וספרות '
      }
    }
    else {
      this.DisplayMessage = 'הרישום בוצע בהצלחה!'
      localStorage.setItem("logged", "true")
      localStorage.setItem("loggedEmail", this.form.email)
      GetUserParameterToLocalStorage(this.form.email)
      this.form = new SignupForm('', '', '', '','','','','','')
    }
  }
}
