export class Survey {

    constructor (
        public CFD : number,
        public closePosition: number,
        public MaxPosition: number,
        public pastExperience: boolean ,
        public treeSayings : number,
        public name: string,
        public mail: string,
        public getMail: boolean,
        public totalAmount : string) {}
    
/**
 * calc_points
 */
public calc_points() {
    let sum = 0
    sum = sum + 10
    let wrongAns
    if (this.pastExperience== true)
    {
        sum = sum + 30
        wrongAns.pastExperience = true
    }
    else{
        wrongAns.pastExperience = false
    }
    if (this.treeSayings == 1)
    {
        sum = sum + 20
        wrongAns.treeSayings = true
    }
    else{
        wrongAns.treeSayings = false
    }
    if (this.MaxPosition == 3)
    {
        sum = sum + 15
        wrongAns.MaxPosition = true
    }
    else
    {
        wrongAns.MaxPosition = false
    }
    if (this.closePosition==1)
    {
        sum = sum +15
        wrongAns.closePosition = false
    }
    else
    {
        wrongAns.closePosition = false
    }
    if (this.CFD != 1)
    {sum = sum+ 10
    wrongAns.cfd = true}
    else
    {
        wrongAns.cfd = false
    }
    
    return {
        "points": sum,
        "wrongAns":wrongAns
    }
}
}
