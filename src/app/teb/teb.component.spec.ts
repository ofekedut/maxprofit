import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TebComponent } from './teb.component';

describe('TebComponent', () => {
  let component: TebComponent;
  let fixture: ComponentFixture<TebComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TebComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TebComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
